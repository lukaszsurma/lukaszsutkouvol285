﻿using UnityEngine;
public class Drag : MonoBehaviour
{
	public LayerMask dragLayers;
	[SerializeField] private float damping = 1.0f;
	[SerializeField] private float frequency = 5.0f;
	public bool drawDragLine = true;
	private TargetJoint2D targetJoint2D;

	void Update ()
	{
		var worldPos = Camera.main.ScreenToWorldPoint (Input.mousePosition);

		if (Input.GetMouseButtonDown(0))
		{
			var collider = Physics2D.OverlapPoint(worldPos, dragLayers);
			if (!collider)
				return;

			var body = collider.attachedRigidbody;
			if (!body)
				return;

			targetJoint2D = body.gameObject.AddComponent<TargetJoint2D> ();
			targetJoint2D.dampingRatio = damping;
			targetJoint2D.frequency = frequency;

			targetJoint2D.anchor = targetJoint2D.transform.InverseTransformPoint(worldPos);		
		}
		else if(Input.GetMouseButtonUp(0))
		{
			Destroy(targetJoint2D);
			targetJoint2D = null;
			return;
		}

		if(targetJoint2D)
		{
			targetJoint2D.target = worldPos;
		}
	}
}