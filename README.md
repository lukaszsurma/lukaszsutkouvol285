# Zadanie rekrutacyjne

  

Moim zdaniem warto używać **object pooling**, aczkolwiek w tak małym zadaniu użycie tej fukncji nie będzie miało większego sensu.
Gdy pracujemy nad bardziej rozbudowanym projektem jak najbardziej warto go używać. 
W unity funkcja **INSTANTIATE** ma słabszą wydajność dlatego w większości przypadków szybciej jest stworzyć zbiór i odciążyć CPU co zwiększy performance naszego projektu. 
W grze gdzie wiemy że na ekranie część projektów lepiej zoptymalizować na przykład linię pocisków, które są nieustannie niszczone warto zastosować **pool objects**, aby móc ponownie wykorzystywać te same obiekty.

# Autor: 
Łukasz Surma