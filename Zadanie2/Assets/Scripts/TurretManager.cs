﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TurretManager : MonoBehaviour
{
    private static TurretManager instance;
    public static TurretManager Instance
    {
        get
        {
            if(instance == null)
            {
                GameObject gameObject = new GameObject("TurretManager");
                gameObject.AddComponent<TurretManager>();
            }
            return instance;
        }
    }
    void Awake()
    {
        instance = this;
    }

    public Text turretCountText;
    public List<GameObject> turretList;
    public void AddTurretToList(GameObject turret)
    {
        turretList.Add(turret);
    }
    public void RemoveTurretFromList(GameObject turret)
    {
        turretList.Remove(turret);
    }

    void Update()
    {
        turretCountText.text = "Towers: "+turretList.Count.ToString();
    }


}
