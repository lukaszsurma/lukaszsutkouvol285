﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public GameObject turretPrefab;
    public Rigidbody2D rb;
    private int randomInt;
    private bool wasTurretHit;

    void OnTriggerEnter2D(Collider2D col)
    {
       if(col.gameObject.tag == "Turret")
        {
            wasTurretHit = true;
            TurretManager.Instance.RemoveTurretFromList(col.gameObject);
            Destroy(col.gameObject);
            Destroy(gameObject);
        }
    }

    void Start()
    {
        
        randomInt = Random.Range(1,4);
        Invoke("Destroy",randomInt);
    }
    void Update()
    {
        rb.velocity = transform.up * 5f;
    }
    void Destroy()
    {
        if(!wasTurretHit)
        {
            if(TurretManager.Instance.turretList.Count < 100)
            {
                GameObject turret = Instantiate(turretPrefab,transform.position,Quaternion.identity);
                turret.GetComponent<Turret>().isNewTurret = true;
            }
            Destroy(gameObject);
        }
      
    }
}
