﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    public GameObject bulletPrefab;
    public Transform shootPointTransform;

    public bool isNewTurret{private get; set;}
    private SpriteRenderer spriteRenderer;
    private int randomDegree;
    private int ammo = 12;
    private float timeToDelay;
    private float timeToStartShoot;
    

    void Start()
    {
        TurretManager.Instance.AddTurretToList(gameObject);
        spriteRenderer = GetComponent<SpriteRenderer>();
        timeToDelay = Time.time + 0.5f;
        timeToStartShoot = Time.time + 6f;
    }

    void Update()
    {
        if(isNewTurret)
        {  
           if(Time.time > timeToStartShoot)
           {
               TurretShoot();
               isNewTurret = false;
           }
        }
        else
        {
            TurretShoot();
        }

        if(TurretManager.Instance.turretList.Count == 100)
        {
            ammo = 12;
        }
        
    }

    void TurretShoot()
    {
        if(Time.time >= timeToDelay && ammo > 0)
        {
            randomDegree = Random.Range(15,45);
            transform.rotation = Quaternion.Euler(Vector3.forward * randomDegree);
            GameObject bullet = Instantiate(bulletPrefab, shootPointTransform.position, shootPointTransform.rotation);
            Rigidbody2D bulletRb = bullet.GetComponent<Rigidbody2D>();
            bulletRb.AddForce(shootPointTransform.up * 4f, ForceMode2D.Impulse);
            ammo --;

            timeToDelay = Time.time + 0.5f;
        }


        if(ammo > 0 || isNewTurret)
        {
            spriteRenderer.color = new Color32(255,0,0,255);
        }
        else
        {
            spriteRenderer.color = new Color32(255,255,255,255);
        }
    }
}
